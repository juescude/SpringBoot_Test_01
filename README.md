# SpringBoot_Test_01

# Authentication
* http://localhost:8081/api/services/login
* Bearer Token

### Roles
* CLIENT: withUser("app").password("app")
* B2B: withUser("b2b").password("b2b")


### Health check

* http://localhost:8081/api/services/utils/v1/app/healthCheck
* http://localhost:8081/api/services/utils/v1/b2b/healthCheck

### Response Exception Handler
* http://localhost:8081/api/v01/services/utils/v2/app/exception?trowException=true
* http://localhost:8081/api/v01/services/utils/v2/app/exception?trowException=false

#Docker

build your the docker image of your application using the following command

* docker build -t demoapp .

to run the image, use the command:

* docker run -p8081:8081 -name demoapp

