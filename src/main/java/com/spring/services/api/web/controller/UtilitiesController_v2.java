package com.spring.services.api.web.controller;

import com.spring.services.api.services.kafka.MessageService;
import com.spring.services.api.web.response.GenericResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController()
@RequestMapping("/utils/v2")
public class UtilitiesController_v2 implements IUtilitiesComtroller {

    @Autowired
    MessageService messageService;

    @Override
    public GenericResponse healthCheck() {
        return null;
    }

    @Override
    public GenericResponse healthCheckB2B() {
        return null;
    }

    @GetMapping("/app/exception")
    ResponseEntity<String> testException(@RequestParam(value = "trowException", required = false) Boolean trowException) {
        if (trowException) {
            throw new IllegalStateException("Exception Message: Exception Activated !");
        } else {
            return ResponseEntity.ok()
                    .body("No Exception here !");
        }
    }

    @PostMapping("/app/kafka/produce/topic/{topic}")
    ResponseEntity<Object> produceMessage(@RequestBody String message,
                                          @PathVariable(value = "topic") String topic) {
        messageService.sendMessage(topic, message);
        return ResponseEntity.ok()
                .body("Message set");
    }
}
